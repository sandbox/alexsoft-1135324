  A module allow to use on a site the counter and an informer Yandex Metric
(http://metrika.yandex.ru/).
  The module has the block with an informer. At all pages of a site it will
be added JavaScript. This code is in charge of the count of visits, reviews and
visitors.
  All parameters of the counter may be configured in the interface. This are:
webvisor, clickmap, track links, accurate track bounce, disable of an automatic
indexing, the synchronous or asynchronous counter type, track hash.
  All parameters of the informer may be configured in the interface. This
are: counter type, information type, informer color, gradient, text color, arrow
color.
  Additional parameter of visit can be passing without change JavaScript or
PHP the code. Additional parameters of visit improve detailing of reports of the
metrics. A value of the parameter can be generate to the php code. Additional
parameters "authorized user" And "user login" can be marked in settings.
