<?php

// $Id: $

function yandex_metrika_admin_settings () {
  global $base_url;

  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yandex Metrika settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

    'yandex_metrika_power' => array(
      '#type' => 'radios',
      '#title' => t('Power'),
      '#description' => t('On/OFF Yandex Metrika'),
      '#options' => array(t('On'), t('Off')),
      '#default_value' => variable_get('yandex_metrika_power', 1),
    ),

    'yandex_metrika_id' => array(
      '#type' => 'textfield',
      '#title' => t('Metrika ID'),
      '#description' => t('Enter your Metrika ID'),
      '#default_value' => variable_get('yandex_metrika_id', ''),
    ),
  );

  $form['counter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Counter settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

    'yandex_metrika_webvisor' => array(
      '#type' => 'checkbox',
      '#title' => t('Webvisor'),
      '#description' => t('Mark this checkbox if you have webvisor.'),
      '#default_value' => variable_get('yandex_metrika_webvisor', 1),
    ),

    'yandex_metrika_click_map' => array(
      '#type' => 'checkbox',
      '#title' => t('Click map'),
      '#description' => t('Mark this checkbox if you have click map.'),
      '#default_value' => variable_get('yandex_metrika_click_map', 1),
    ),

    'yandex_metrika_external_links' => array(
      '#type' => 'checkbox',
      '#title' => t('External links'),
      '#description' => t('Mark this checkbox if you have external links report.'),
      '#default_value' => variable_get('yandex_metrika_external_links', 1),
    ),

    'yandex_metrika_accurate_track_bounce' => array(
      '#type' => 'checkbox',
      '#title' => t('Accurate track bounce'),
      '#description' => t('Mark this checkbox if you have accurate track bounce.'),
      '#default_value' => variable_get('yandex_metrika_accurate_track_bounce', 1),
    ),

    'yandex_metrika_noindex' => array(
      '#type' => 'checkbox',
      '#title' => t('Disable autoindex'),
      '#description' => t('Mark this checkbox if you have disable autoindex.'),
      '#default_value' => variable_get('yandex_metrika_noindex', 0),
    ),
  );

  $form['additional'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,

    'yandex_metrika_asynchronous' => array(
      '#type' => 'checkbox',
      '#title' => t('Asynchronous'),
      '#description' => t('Mark this checkbox if you have asynchronous.'),
      '#default_value' => variable_get('yandex_metrika_asynchronous', 1),
    ),

    'yandex_metrika_track_hash' => array(
      '#type' => 'checkbox',
      '#title' => t('Track hash'),
      '#description' => t('Mark this checkbox if you have track hash.'),
      '#default_value' => variable_get('yandex_metrika_track_hash', 0),
    ),
  );

  $form['parameters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,

    'yandex_metrika_send_login' => array(
      '#type' => 'checkbox',
      '#title' => t('Send site user login'),
      '#description' => t('Mark this checkbox if you have send to counter site user login.'),
      '#default_value' => variable_get('yandex_metrika_send_login', 0),
    ),

    'yandex_metrika_guest_user_sort' => array(
      '#type' => 'checkbox',
      '#title' => t('Send guest or user'),
      '#description' => t('Mark this checkbox if you have send to counter guest or user.'),
      '#default_value' => variable_get('yandex_metrika_guest_user_sort', 0),
    ),
  );

  $form = system_settings_form($form);
  return $form;
}
